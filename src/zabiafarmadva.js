class Zaba {
    constructor(prejedene) {         //toto priraduje atributy
        this.zjedene = 0;
        this.prejedene = prejedene 
             
    }
    ham(mucha) {
        this.zjedene = this.zjedene + mucha;

    }
    zijes() {     //metoda
        return this.zjedene < this.prejedene;       //toto má takú istu funkciu ako to pod nim
    }
    //     if (this.zjedene < this.prejedene) {       
    //         return true;          //ak spapa menej ako 20 tak žije
    //     } 
    //     else {
    //         return false;       //ak spapa viac, je mrtva
    //     }
    // }
}

function getRandomInt(max) {    //dá mi random number
    return Math.floor(Math.random() * Math.floor(max));
  }
  


const terarium = []         //terarko je prazdne
for (let i = 0; i < 10; i++) {          //vytvori zaby
   const zivot = getRandomInt(20);      //konstante zivot priradi nahodne cislo z funkcie nahodnych cisel
   const z = new Zaba(zivot);           // konstante z priradi zaby s konstantou zivot ako parameter constructora
   terarium.push(z);       //pridáva do pola "terarium" novovytvorene zaby
}
console.log(terarium);

// terarium.forEach(zaba => zaba.ham(7));          //krmenie
// console.log(terarium);

//teraz zistime kolko ich zije
// const zijuce = terarium
//     .map(zaba => zaba.zijes())
//     .filter(zijes => zijes)
//     .length;
// console.log(zijuce);

// for (let i = 0; i < 10; i++) {
//     terarium.forEach(zaba => zaba.ham(3));          //krmenie
//     const zijuce = terarium
//         .map(zaba => zaba.zijes())
//         .filter(zijes => zijes)
//         .length;
//     console.log(i, zijuce);     //kolkata je to iteracia
// }

// takto urobis to krmenie v jednom cykle dowhile
// let prezili
// do {
//     terarium.forEach(zaba => zaba.ham(3));
//     prezili = terarium
//         .map(zaba => zaba.zijes())
//         .filter(zijes => zijes)
//         .length;
//     console.log(prezili); 
// } while (prezili > 0);


//teraz to refaktorujee - krmenie a pocitanie spravime ako funkcie
//refaktoring spustis tym ze oznacis co chces dat ako fciu a stlac shift+ctrl+r
// let prezili
// do {
//     krmenie();
//     prezili = pocitanie();
//     console.log(prezili); 
// } while (prezili > 0);

// function pocitanie() {
//     return terarium
//         .map(zaba => zaba.zijes())
//         .filter(zijes => zijes)
//         .length;
// }

// function krmenie() {
//     terarium.forEach(zaba => zaba.ham(3));
// }

let prezili
do {
    krmenie();
    prezili = pocitanie();
    console.log(prezili); 
} while (prezili > 0);

function pocitanie() {
    return terarium
        .filter(zaba => zaba.zijes())       //bez pouzitia .map
        .length;
}

function krmenie() {
    terarium.forEach(zaba => zaba.ham(3));
}