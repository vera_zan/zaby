const l = [1, 2, 3, "muhehe"];  //ciarkou oddelený zoznam..? je jedno aké objekty sú v []


for (let i = 0; i < l.length; i++) {
    const element = l[i];
    console.log(element);

}
// takto sa iteruje cez zoznam, ten mozes chapat ako vektor, stlpec v tabulke atd
// vies spravit zoznam zoznamov

const k = [[1, 2], [3, 4]];

const d = [];

for (let i = 0; i < k.length; i++) {        //for i cyklus
    const matica = k[i];
    console.log(matica);
    d.push(matica); // po jednom presunie prvky matice do prazdneho d-cka
    
}
console.log(d);

for (const element of d) {  //alebo mozes vynechat to hore a spravit to len takto jednoducho //for of cyklus
    console.log(element);       // ak to chces vypisovať pre maticu tak miesto d bude k 
    
}
