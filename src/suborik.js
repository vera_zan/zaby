console.log("ahoj");

const a = 11;
let b;
b = 8
console.log("a = ", a, "\n", "b = ", b)

console.log(`hocičo ${a}`);
//toto sa používa na vypisovanie
//control flow ? prikazy pre cykly

if (a) {
    console.log("niečo sem napíš ak a je pravda");

} else {
    console.log("a nie je pravda");

}
// a ak je cislo tak ho vyhodnotí ako false len ak by bolo = 0
//toto bol cyklus a teraz ideme na nekonečný cyklus

let x;
x = 10;

while (x > 0) {
    x = x - 1;
    console.log(x);

}
//toto je nekonecny cyklus ktory sleduje jednu podmienku a podla nej sa zariadi, ak je v podmienke true tak sa bude tocit do nekonecna
// x=x-1 je "dekrementované x" :)
//vraj x=x-1 je to isté ako x -= 1

for (let i = 0; i < 10; i++) {  // ak tam dáš const nie let tak bude protestovať
    console.log(i);
}

const l = [1, 2, 3, "muhehe"];  //ciarkou oddelený zoznam..? je jedno aké objekty sú v []
// constantnej sme priradili zoznam (list literal)
console.log(l)  // toto tu treba aby to vypísal v terminály

for (let i = 0)