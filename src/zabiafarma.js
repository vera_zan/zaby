class Zaba {
    constructor() {         //toto priraduje atributy
        this.zjedene = 0; 
             //this je referencia na tu konkretnu instanciu 
                     //da je jej zivotnost obmedzit aj tak ze napisem this.prezranie = prezranie, ale aj do porametra constructora musim dat (prezranie)
    }
    ham(mucha) {
        this.zjedene = this.zjedene + mucha;

    }
    zijes() {     //metoda
        return this.zjedene < 20;       //toto má takú isu kunkciu ako to pod nim
    }
    //     if (this.zjedene < 20) {       
    //         return true;          //ak spapa menej ako 20 tak žije
    //     } 
    //     else {
    //         return false;       //ak spapa viac, je mrtva
    //     }
    // }
}

const zaba1 = new Zaba    //identifikator premennej, instancia, vytvorí novy objekt
console.log(zaba1);
zaba1.ham(19);

console.log(zaba1.zijes());

const terarium = []         //terarko je prazdne
for (let i = 0; i < 10; i++) {
   const z = new Zaba();
   terarium.push(z);       //pridáva do pola
}
console.log(terarium);

for (let i = 0; i < terarium.length; i++) {
    const zaba = terarium[i];
    zaba.ham(5);
    console.log(zaba.zijes());      //vypíše či zije zaba - konstanta, zadefinovaná v rámci cyklu

}
console.log(terarium);      //vypíše všetky žaby v teráriu

const ziju = terarium.map(function (z) {       //terarium je pole a map je fcia pola
    return z.zijes();                           // mapujeme stav aký maju tie zaby - takze to aj rovno do vystupneho pola npise ako sa maju tie zaby
})
console.log(ziju);          //tato fcia rovno vrati dalsie pole

terarium.forEach(function (z) {       //foreach to spravi nad .. a co ja viem
         //nema vystpovu hodnotu takze tam nebude ziadny log na vypisanie potrebny.. takymto stylom by sa dalo spravit aj krmenie, cez forEach
})
 // da sa aj filtrovacie kriteium , kde by mi do noveho pola vypisalo len tie ktore ziju alebo su mrtve..ofiltruje mi ich..
 //krmime vsetky zaby cez forEach..juhuu
 terarium.forEach(function(zaba) {zaba.ham(7)})
 console.log(terarium);
 
 terarium.forEach(zaba => zaba.ham(7))        //toto je to iste ako to hore, ibyze miesto "function" je tam arrow a zaba nemusi byt (zaba) - v zatvorkach, lebo je to len jeden parameter, ked by ich bolo viac tak by boli v zatvorkach...
 console.log(terarium);

 //počet zijucich
const pocetzijucich = terarium
    .map(zaba => zaba.zijes())
    .filter(zijes => !zijes)            //! negacia,vypise ti ektore neziju
    .length;
console.log(pocetzijucich);
