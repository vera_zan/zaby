//slovnik je slovoalebokľúč: hodnota
const o = { x: 1, y: 2 };
console.log(o.y);   //takto vytiahneš slovo z objektu, tu sa compilator presvedčí či tam je ten atribút

console.log(o["y"]);    //to isté ale ak dáš [] tak to slovo musíš definovať ako string, tu sa nepresvedčí či existuje

const okeys = Object.keys(o);    //toto je proste nejaky prikaz ako console.log, potrebuje mat zadefinovanu premennu pre ten dictionary o a to sa zadefiinuje cez Object.keys()
console.log(okeys);

for (let i = 0; i < okeys.length; i++) {
    const key = okeys[i];
    console.log(key, o[key]);
    
}

for (const key in o) {  //alebo mozes vynechat to hore a spravit to len takto jednoducho
    console.log(key);
    
}
